package com.nlmk.dezhemesov.taskmanager.enumerated;

/**
 * Перечисление ролей
 */
public enum Role {

    ADMIN(true),
    USER(false);

    /**
     * Признак администраторских полномочий
     */
    private static final boolean isAdmin = false;

    /**
     * Установка признака администраторских полномочий
     *
     * @param isAdmin значение признака
     */
    Role(boolean isAdmin) {
        isAdmin = isAdmin;
    }

    /**
     * Получение признака администраторских полномочий
     *
     * @return значение признака
     */
    public boolean isAdmin() {
        return isAdmin;
    }
}
