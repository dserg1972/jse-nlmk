package com.nlmk.dezhemesov.taskmanager.util;

import com.nlmk.dezhemesov.taskmanager.entity.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс для работы с хэш-функциями
 */
public class Hash {

    /**
     * Префикс соли
     */
    private final static String SALT_PREFIX = "e$xVP?X=E1E0hGj=";
    /**
     * Постфикс соли
     */
    private final static String SALT_POSTFIX = "U4JAS+R02Vs26tY&";
    /**
     * Количество итерации с солью
     */
    private final static int SALT_ITERATIONS = 60000;

    /**
     * Вычисление хэш-кода для строки
     *
     * @param value исходная строка
     * @return хэш-значение
     */
    public static String generateMD5(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            return Base64.getEncoder().encodeToString(messageDigest.digest(value.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, "Unknown hash algorithm", e);
        }
        return null;
    }

    /**
     * Вычисление хэш-кода для строки с солью
     *
     * @param value исходная строка
     * @return хэш-значение
     */
    public static String generateMD5withSalt(String value) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, "Unknown hash algorithm", e);
            return null;
        }
        messageDigest.update(SALT_PREFIX.getBytes());
        messageDigest.update(value.getBytes());
        messageDigest.update(SALT_POSTFIX.getBytes());
        byte[] digest = messageDigest.digest();
        for (int i = 0; i < SALT_ITERATIONS; i++) {
            messageDigest.update(SALT_PREFIX.getBytes());
            messageDigest.update(digest);
            messageDigest.update(SALT_POSTFIX.getBytes());
            digest = messageDigest.digest();
        }
        return Base64.getEncoder().encodeToString(digest);
    }

    public static void main(String[] args) {
        System.out.println(generateMD5withSalt("djfgbhegbhierhgehg 868698"));
    }
}
