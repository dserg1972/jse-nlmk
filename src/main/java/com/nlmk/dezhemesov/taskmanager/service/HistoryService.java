package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.repository.HistoryRepository;

import java.util.List;

/**
 * Служба истории команд
 */
public class HistoryService {

    /**
     * Ссылка на репозиторий истории
     */
    private HistoryRepository historyRepository;

    /**
     * Конструктор
     *
     * @param historyRepository ссылка на репозиторий истории
     */
    public HistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    /**
     * Записать команду в историю
     *
     * @param command текст команды
     */
    public void put(String command) {
        historyRepository.create(command);
        if (historyRepository.size() > 10)
            historyRepository.removeFirst();
    }

    /**
     * Получить список команд
     *
     * @return список команд в истории
     */
    public List<String> findAll() {
        return historyRepository.findAll();
    }

    /**
     * Очистить историю
     */
    public void clear() {
        historyRepository.clear();
    }

}
