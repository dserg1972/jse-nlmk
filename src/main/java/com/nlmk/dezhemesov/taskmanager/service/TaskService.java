package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.entity.Task;
import com.nlmk.dezhemesov.taskmanager.repository.TaskRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Служба задач
 */
public class TaskService {

    /**
     * Репозиторий задач
     */
    private final TaskRepository taskRepository;

    /**
     * Конструктор
     *
     * @param taskRepository ссылка на репозиторий задач
     */
    public TaskService(final TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Создание задачи и помещение её в репозиторий
     *
     * @param name имя задачи
     * @return созданная задача
     */
    public Task create(final String name) {
        return taskRepository.create(name);
    }

    /**
     * Создание задачи с именем и описанием и добавление в хранилище
     *
     * @param name        имя задачи
     * @param description описание задачи
     * @return созданная задача
     */
    public Task create(final String name, final String description) {
        return taskRepository.create(name, description);
    }

    /**
     * Очистка репозитория задач
     */
    public void clear() {
        taskRepository.clear();
    }

    /**
     * Получение списка задач
     *
     * @return список задач в репозитории
     */
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Получить отсортированный по имени список задач
     *
     * @return список задач
     */
    public List<Task> findSortedByName() {
        return taskRepository.findAll().stream().sorted((t1, t2) -> t1.getName().compareToIgnoreCase(t2.getName())).collect(Collectors.toList());
    }

    /**
     * Поиск задачи по индексу в репозитории
     *
     * @param index индекс задачи в репозитории
     * @return найденная задача либо null, если не найдена
     */
    public Task findByIndex(final int index) {
        if (index < 0 || index > taskRepository.size())
            return null;
        return taskRepository.findByIndex(index);
    }

    /**
     * Поиск задачи по имени
     *
     * @param name имя задачи
     * @return найденная задача либо null, если не найдена
     */
    public Task findByName(final String name) {
        if (name == null || name.equals(""))
            return null;
        return taskRepository.findByName(name);
    }

    /**
     * Поиск задачи по идентификатору
     *
     * @param id идентификатор задачи
     * @return найденная задача либо null, если не найдена
     */
    public Task findById(final Long id) {
        if (id == null)
            return null;
        return taskRepository.findById(id);
    }

}
