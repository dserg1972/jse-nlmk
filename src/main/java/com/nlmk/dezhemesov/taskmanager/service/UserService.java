package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.entity.User;
import com.nlmk.dezhemesov.taskmanager.enumerated.Role;
import com.nlmk.dezhemesov.taskmanager.exception.DuplicateUserException;
import com.nlmk.dezhemesov.taskmanager.repository.UserRepository;

import java.util.List;

/**
 * Служба пользователей
 */
public class UserService {
    /**
     * Репозиторий пользователей
     */
    private final UserRepository userRepository;

    /**
     * Конструктор
     *
     * @param userRepository репозиторий пользователей
     */
    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Создание учётной записи и помещение в репозиторий
     *
     * @param login    имя учетной записи
     * @param password пароль
     * @param role     роль
     * @return созданная учётная запись
     * @throws DuplicateUserException в случае обнаружения дублирующей записи
     */
    public User create(final String login, final String password, final Role role) throws DuplicateUserException {
        if (login == null || login.isEmpty())
            return null;
        final User findUser = userRepository.findAll().stream().filter(u -> u.getLogin().equals(login)).findFirst().orElse(null);
        if (findUser != null)
            throw new DuplicateUserException();
        return userRepository.create(login, password == null ? "" : password, role);
    }

    /**
     * Поиск учетной записи по имени
     *
     * @param login имя учётной записи
     * @return учётная запись
     */
    public User findByLogin(final String login) {
        return userRepository.findByLogin(login);
    }

    /**
     * Получение списка учётных записей
     *
     * @return список учётных записей
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Поиск пользователя по идентификатору
     *
     * @param id ИД пользователя
     * @return
     */
    public User findById(final Long id) {
        return userRepository.findById(id);
    }

    /**
     * Удаление пользователя
     *
     * @param login имя учётной записи
     * @return пользователь либо null, если такого нет
     */
    public User removeByLogin(final String login) {
        return userRepository.removeByLogin(login);
    }

}
