package com.nlmk.dezhemesov.taskmanager.repository;

import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.util.Identifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Хранилище проектов
 */
public class ProjectRepository {

    /**
     * Список проектов
     */
    private final List<Project> projects = new ArrayList<>();

    /**
     * Идентификатор текущего пользователя
     */
    private Identifier userId = null;

    /**
     * Конструктор
     *
     * @param userId идентификатор пользователя
     */
    public ProjectRepository(final Identifier userId) {
        this.userId = userId;
    }

    /**
     * Создание проекта с именем и добавление в хранилище
     *
     * @param name имя проекта
     * @return созданный проект
     */
    public Project create(final String name) {
        final Project project = new Project(name,userId.getIdentifier());
        projects.add(project);
        return project;
    }

    /**
     * Создание проекта с именем и описанием и добавление в хранилище
     *
     * @param name        имя проекта
     * @param description описание проекта
     * @return созданный проект
     */
    public Project create(final String name, final String description) {
        final Project project = new Project(name, userId.getIdentifier(), description);
        projects.add(project);
        return project;
    }

    /**
     * Очистка хранилища
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Возврат ссылки на подмножество проектов, принадлежащих текущему пользователю
     *
     * @return ссылка на хранилище
     */
    public List<Project> findAll() {
        return projects.stream().filter(p->p.getUserId().equals(userId.getIdentifier())).collect(Collectors.toList());
    }

    /**
     * Поиск проекта в хранилище по индексу
     *
     * @param index индекс проекта в хранилище
     * @return найденный проект либо null, если проект не найден либо неправильный индекс
     */
    public Project findByIndex(final int index) {
        return findAll().get(index);
    }

    /**
     * Поиск проекта в хранилище по имени
     *
     * @param name имя проекта
     * @return найденный проект либо null, если проект не найден
     */
    public Project findByName(final String name) {
        Optional<Project> optionalProject = findAll().stream().filter(p -> name.equals(p.getName())).findFirst();
        return optionalProject.orElse(null);
    }

    /**
     * Поиск проекта в хранилище по идентификатору
     *
     * @param id идентификатор проекта
     * @return найденный проект либо null, если проект не найден
     */
    public Project findById(final Long id) {
        Optional<Project> optionalProject = findAll().stream().filter(p -> id.equals(p.getId())).findFirst();
        return optionalProject.orElse(null);
    }

    /**
     * Получение размера хранилища
     *
     * @return Размер хранилища
     */
    public int size() {
        return findAll().size();
    }

}
